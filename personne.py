import io
import yaml

class Personne:
    def __init__(self, prenom=None, nom=None, adresse=None):
        self.prenom=prenom
        self.nom=nom
        self.adresse=adresse

    @staticmethod
    def from_file(fic):
        res = []
        #with open(fic, 'r') as flux:
        #    personnes = yaml.load(flux)
        flux = open(fic, 'r')
        personnes = yaml.load(flux)
        
        for p in personnes:
            nom = p.get("nom")
            prenom = p.get("prenom")
            adresse = p.get("adresse")
            res.append(Personne(prenom, nom, adresse))
        return res

    def __repr__(self):
        return "Personne :\n %s, %s, %s\n\n" %(self.prenom, self.nom, self.adresse)

lp = Personne.from_file("personnes.yaml")
for p in lp:
    print(p)


class Test:
    def __init__(self):
        self.test = "test"

t = Test()
print(t.get("test"))