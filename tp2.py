class Box:
    def __init__(self):
        self._contents = []
        self._open = False
        self._capacity = None
        self._key = None

    def set_capacity(self, capacity):
        self._capacity = capacity

    def capacity(self):
        return self._capacity

    def add(self, truc):
        self._contents.append(truc)

    def __contains__(self, truc):
        return truc in self._contents

    def remove(self, truc):
        self._contents.remove(truc)

    def open(self):
        if self._key == None:
            self._open = True

    def close(self):
        if self._key == None:
            self._open = False

    def is_open(self):
        return self._open

    def action_look(self):
        res = "La boite est fermee"
        if self._open:
            res = "La boite contient: " + ", ".join(self._contents)
        return res

    def has_room_for(self, thing):
        res = True
        if self._capacity != None:
            res = thing.volume() <= (self._capacity)
        return res

    def action_add(self, thing):
        if self.has_room_for(thing) and self.is_open():
            self.add(thing)
            self._capacity -= thing.volume()

    def set_key(self, thing):
        self._key = thing

    def open_with(self, key):
        if self._key == key:
            self._open = True

    def close_with(self, key):
        if self._key == key:
            self._open = False

    def volume(self):
        return self._capacity

class Thing:

    def __init__(self, size=None):
        self._size = size
        self._name = None

    def __repr__(self):
        return self.name

    def set_name(self, name):
        self._name = name

    def volume(self):
        return self._size

    def has_name(self, name):
        return self._name == name
