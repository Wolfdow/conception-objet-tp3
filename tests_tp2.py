from tp2 import *

def test_box_create():
    b = Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "truc2" in b

def test_remove():
    b = Box()
    b.add("truc1")
    b.remove("truc1")
    assert "truc1" not in b

def test_open():
    b = Box()
    b.close()
    assert b.is_open() == False
    b.open()
    assert b.is_open()

def test_action_look():
    b = Box()
    b.add("a")
    b.add("b")
    b.open()
    assert b.action_look() == "La boite contient: a, b"
    b.close()
    assert b.action_look() == "La boite est fermee"

def test_box_capacity():
    b = Box()
    b.set_capacity(5)
    assert b.capacity() == 5

def test_capacity_default():
    b = Box()
    assert b.capacity() == None

def test_has_room_for():
    t1 = Thing(2)
    t2 = Thing(6)
    b = Box()
    assert b.has_room_for(t2)
    b.set_capacity(5)
    assert b.has_room_for(t1)
    assert b.has_room_for(t2) == False

def test_thing_create():
    t = Thing(3)

def test_volume():
    t = Thing(3)
    assert t.volume() == 3

def test_action_add():
    b = Box()
    b.open()
    b.set_capacity(5)
    t = Thing(5)
    b.action_add(t)
    assert t in b
    b2 = Box()
    b2.set_capacity(6)
    t2 = Thing(7)
    b2.action_add(t2)
    assert t2 not in b2
    b = Box()
    b.close()
    b.set_capacity(5)
    t3 = Thing(5)
    b.action_add(t3)
    assert t3 not in b

def test_has_name():
    t4 = Thing(5)
    t4.set_name("a")
    assert t4.has_name("a")

def test_key():
    key = Thing(4)
    b = Box()
    b.open()
    assert b.is_open()
    b.set_key(key)
    b.close_with(key)
    b.open()
    assert b.is_open() == False
    b.open_with(key)
    assert b.is_open()

def test_boite():
    b = Box()
    b.open()
    b2 = Box()
    b.set_capacity(5)
    b2.set_capacity(3)
    b.action_add(b2)
    assert b2 in b
